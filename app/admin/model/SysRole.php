<?php
/**
 * Created by Shy
 * Date 2020/12/3
 * Time 18:02
 */


namespace app\admin\model;

use think\facade\Cache;
use think\facade\Db;

class SysRole extends BasicModel
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    /**
     * 获取我的菜单
     * User: 万奇
     * Date: 2021/2/7 0007
     * @param $user_info
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function get_menu_list($user_info){
        $where[]        = ['is_menu', '=', 1];
        $where[]        = ['disable', '=', 0];
        if ($user_info['role_id'] != '0'){
            $where[]    = ['id', 'in', $this->where(['id' => $user_info['role_id']])->value('module_id')];
        }

        $result         = array_grouping(Db::name('sys_module')->field('title,url,icon,code,parent_code')->where($where)->order(['sort' => 'asc'])->select()->toArray(), 'parent_code');

        // 通过一级目录树状结构查询(无限级)
        $result = category_group($result, $result[0], 'group', 'code');

        return $result;
    }

    /**
     * 获取一条角色信息
     * User: 万奇
     * Date: 2021/2/5 0005
     * @param $param
     * @return array|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function show_roles_info($param){
        $result         = $this->where(['id' => $param['id']])->find();
        $result['module_id']        = explode(',', $result['module_id']);

        return $result;
    }

    /**
     * 获取角色列表
     * User: 万奇
     * Date: 2021/2/5 0005
     * @param $user_info
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function get_roles_list($user_info){
        return $this->field('id,name,disable')->order(['create_at' => 'desc'])->select()->toArray();
    }

    /**
     * 新增 编辑 删除角色
     * User: 万奇
     * Date: 2021/2/5 0005
     * @param $param
     * @param $user_info
     * @return bool
     */
    public function roles_add($param, $user_info){
        // 删除
        if (is_exists($param['del'])){
            $is_user    = Db::name('sys_user')->where(['role_id' => $param['id']])->count();
            if ($is_user){
                response(500, '该角色下存在账户，无法删除');
            }

            $result     = $this->where(['id' => $param['id']])->delete();
            Cache::delete('role_module_list_' . $param['id']);
            return $result;
        }

        if (is_exists($param['id'])){
            $this->where(['id' => $param['id']])->strict(false)->update($param);
            if ($param['disable'] == 1){
                Cache::delete('role_module_list_' . $param['id']);
            }
        } else{
            $param['id']        = uuid();
            $this->save($param);
        }

        $role_module_list       = Db::name('sys_module')->where([['id', 'in', $param['module_id']]])->column('url', 'url');

        Cache::set('role_module_list_' . $param['id'], $role_module_list);
    }

}