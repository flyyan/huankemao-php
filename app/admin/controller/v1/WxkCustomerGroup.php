<?php
/**
 * 客户群
 * User: 万奇
 * Date: 2021/10/9 17:48
 */

namespace app\admin\controller\v1;


use app\admin\model\CmsContentEngine;
use think\App;

class WxkCustomerGroup extends BasicController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * 编辑群二维码
     * User: 万奇
     * Date: 2021/2/24 0024
     * @throws \think\db\exception\DbException
     */
    public function edit_customer_group_qr_code(){
        param_receive(['id', 'url', 'media_id']);

        $customer = new \app\admin\model\WxkCustomerGroup();
        $customer->edit_customer_group_qr_code($this->param);

        response(200, '操作成功');
    }

    /**
     * 上传群二维码
     * User: 万奇
     * Date: 2021/2/24 0024
     */
    public function upload_group_qr_code(){
        $image          = request()->file('image');

        if (!$image){
            response(500, '请选择二维码');
        }

        // 本地上传
        $url        = upload_photo('image');

        //上传至企业微信
        $model      = new CmsContentEngine();
        $result     = $model->upload_qy_material($_FILES['image']['tmp_name'], $_FILES['image']['name'], 'image');

        response(200, '操作成功', ['url' => $url, 'media_id' => $result['media_id']]);
    }

    /**
     * 客户群列表详情
     * User: 万奇
     * Date: 2021/2/24 0024
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function get_customer_group_info(){
        param_receive(['id', 'page', 'limit']);

        $customer = new \app\admin\model\WxkCustomerGroup();
        $result = $customer->get_customer_group_info($this->param);

        response(200, '', $result['data'], $result['total']);
    }

    /**
     * 客户群列表
     * User: 万奇
     * Date: 2021/2/24 0024
     * @throws \think\db\exception\DbException
     */
    public function get_customer_group_list(){
        param_receive(['page', 'limit']);

        $customer = new \app\admin\model\WxkCustomerGroup();
        $result = $customer->get_customer_group_list($this->param);

        response(200, '', $result['data'], $result['total']);
    }



}