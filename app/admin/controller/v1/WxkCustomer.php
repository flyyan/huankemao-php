<?php
/**
 * Created by PhpStorm.
 * User: 万奇
 * Date: 2020/11/29 0029
 * Time: 23:15
 */

namespace app\admin\controller\v1;


use app\admin\model\WxkCustomerFollow;
use app\Request;
use think\App;
use think\facade\Db;

class WxkCustomer extends BasicController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * 客户互动轨迹列表
     * User: 万奇
     * Date: 2021/7/6 17:36
     * @throws \think\db\exception\DbException
     */
    public function get_customer_track_list(){
        param_receive(['external_user_id', 'page', 'limit']);

        $customer = new \app\admin\model\WxkCustomerTrack();
        $result = $customer->get_customer_track_list($this->param);

        response(200, '', $result['data'], $result['total']);
    }

    /**
     * 客户跟进记录列表
     * User: 万奇
     * Date: 2021/3/25 0025
     * @throws \think\db\exception\DbException
     */
    public function get_customer_follow_list(){
        param_receive(['external_user_id', 'follow_userid', 'page', 'limit']);

        $customer = new \app\admin\model\WxkCustomerFollow();
        $result = $customer->get_customer_follow_list($this->param);

        response(200, '', $result['data'], $result['total']);
    }

    /**
     * 添加客户跟进
     * User: 万奇
     * Date: 2021/7/6 17:36
     * @throws \think\db\exception\DbException
     */
    public function add_customer_follow(){
        param_receive(['external_user_id', 'follow_userid', 'add_follow_user', 'follow_type', 'content']);
        $customer = new \app\admin\model\WxkCustomerFollow();
        $customer->add_customer_follow($this->param, 1);

        response(200, '操作成功');
    }

    /**
     * 上传客户画像
     * User: 万奇
     * Date: 2021/7/6 17:36
     */
    public function upload_customer_portrait_photo(){
        $result         = upload_photo('photo');

        response(200, '操作成功', $result);
    }

    /**
     * 回显编辑客户画像
     * User: 万奇
     * Date: 2021/4/30 0024
     */
    public function show_edit_customer_portrait(){
        $customer = new \app\admin\model\WxkCustomer();
        $result = $customer->show_edit_customer_portrait();

        response(200, '', $result);
    }

    /**
     * 获取客户画像
     * User: 万奇
     * Date: 2021/4/28 0023
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function get_customer_portrait(){
        param_receive(['external_user_id']);
        $customer = new \app\admin\model\WxkCustomer();
        $result = $customer->get_customer_portrait($this->param);

        response(200, '', $result);
    }

    /**
     * 修改客户详情
     * User: 万奇
     * Date: 2021/3/23 0023
     * @throws \think\db\exception\DbException
     */
    public function edit_customer_info(){
        param_receive(['follow_userid', 'external_user_id']);
        $customer = new \app\admin\model\WxkCustomer();
        $customer->edit_customer_info($this->param, $this->user_info['user_id'], 1);

        response(200, '操作成功');
    }

    /**
     * 获取客户详情
     * User: 万奇
     * Date: 2021/3/23 0023
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function get_customer_info(){
        param_receive(['id']);
        $customer = new \app\admin\model\WxkCustomer();
        $result = $customer->get_customer_info($this->param);

        response(200, '', $result);
    }

    /**
     * 客户打标签/移除标签
     * User: 万奇
     * Date: 2021/1/15 0015
     */
    public function customer_tagging(){
        param_receive(['id', 'tag_ids', 'type']);
        $customer = new \app\admin\model\WxkCustomer();
        $customer->customer_tagging($this->param, $this->user_info['user_id'], 1);

        response(200, '操作成功');
    }

    /**
     * 客户打标签回显已有的标签
     * User: 万奇
     * Date: 2021/1/14 0014
     */
    public function show_customer_tag(){
        param_receive(['id', 'type']);
        $customer = new \app\admin\model\WxkCustomer();
        $result = $customer->show_customer_tag($this->param);

        response(200, '', $result);
    }

    /**
     * 重复客户列表
     * User: 万奇
     * Date: 2021/1/14 0014
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function repeat_list_customer(){
        param_receive(['page', 'limit']);

        $customer = new \app\admin\model\WxkCustomer();
        $result = $customer->repeat_list_customer($this->param);

        response(200, '', $result['data'], $result['count']);
    }

    /**
     * 客户列表渲染
     * User: 万奇
     * Date: 2021/1/14 0014
     */
    public function show_list_customer(){
        $customer = new \app\admin\model\WxkCustomer();
        $result = $customer->show_list_customer();

        response(200, '', $result);
    }

    /**
     * 客户列表
     * User: 万奇
     * Date: 2020/12/3 0003
     * @throws \think\db\exception\DbException
     */
    public function get_list_customer()
    {
        param_receive(['page', 'limit']);

        $customer = new \app\admin\model\WxkCustomer();
        $result = $customer->get_list_customer($this->param);

        response(200, '', $result['data'], $result['count']);
    }

    /**
     * 同步企业微信客户
     * User: 万奇
     * Date: 2020/12/4 0004
     * @throws \think\db\exception\DbException
     */
    public function synchro_customer()
    {
        // 同步客户标签
        $wechat_user = new \app\admin\model\WxkCustomerTag();
        $wechat_user->synchro_customer_tag();

        // 同步客户
        $wechat_user = new \app\admin\model\WxkCustomer();
        $wechat_user->synchro_customer();

        response(200, '操作成功');
    }
}