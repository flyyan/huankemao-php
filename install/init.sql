/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40101 SET FOREIGN_KEY_CHECKS = 0 */;



INSERT INTO `cms_content_group`(`id`, `name`, `parent_id`) VALUES ('b01f1c2f-d91f-d337-9b3e-f8ab8a9ccba7', '未分组', 0);


-- ----------------------------
-- Database `huankemao`
-- ----------------------------

-- --------------------------------------------------------

-- ----------------------------
-- Init data for sys_app
-- ----------------------------

-- ----------------------------
-- Init data for wxk_config
-- ----------------------------

-- ----------------------------
-- Init data for sys_user
-- ----------------------------

-- ----------------------------
-- Init data for wxk_live_qr_group
-- ----------------------------

INSERT INTO `wxk_live_qr_group`(`id`, `code`, `parent_code`, `name`, `amount`) VALUES ('b01f1c2f-d91f-d337-9b3e-f8ab8a9ccba8', 1, 0, '未分组', 0);


-- ----------------------------
-- Records of sys_module
-- ----------------------------
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a01', 1, 0, '首页', '/home', 'icon-shouye', 0, 1, 0, '2021-02-05 17:01:58', '2021-09-18 15:05:17');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a04', 4, 0, '获客工具', 'live_code', 'icon-erweima', 0, 1, 0, '2021-02-05 17:01:58', '2021-02-06 21:56:52');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a05', 5, 4, '渠道活码', '/live_code/manage', NULL, 0, 1, 0, '2021-02-05 17:01:58', '2021-02-06 21:56:49');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a06', 6, 5, '活码列表', 'get_live_qr_list', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 21:20:51');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a07', 7, 5, '新增编辑删除活码分组', 'add_code_group', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 18:25:39');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a08', 8, 5, '新增编辑活码', 'add_live_qr', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 21:47:28');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a09', 9, 5, '活码移动', 'live_qr_group_move', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 21:56:33');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a10', 10, 5, '删除活码', 'delete_live_qr', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 21:47:28');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a11', 11, 5, '查看成员数', 'get_live_qr_staff', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 21:47:28');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a12', 12, 5, '查看客户数', 'get_live_qr_customer', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 21:47:28');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a13', 13, 0, '客户CRM', 'custom_crm', 'icon-web-icon-', 0, 1, 0, '2021-02-05 17:01:58', '2021-02-19 14:15:45');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a14', 14, 13, '客户', '/custom_crm/manage', NULL, 0, 1, 0, '2021-02-05 17:01:58', '2021-02-06 21:47:28');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a15', 15, 14, '客户列表', 'get_list_customer', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:17:26');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a16', 16, 14, '查看重复客户列表', 'repeat_list_customer', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:17:26');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a17', 17, 14, '客户打标签/移除标签', 'customer_tagging', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:17:26');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a18', 18, 13, '客户标签', '/custom_crm/label', NULL, 0, 1, 0, '2021-02-05 17:01:58', '2021-02-06 22:21:52');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a19', 19, 18, '客户标签列表', 'get_customer_tag', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:17:26');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a20', 20, 18, '编辑删除标签组', 'edit_customer_tag_group', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:17:26');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a21', 21, 18, '编辑删除客户标签', 'edit_customer_tag', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:17:26');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a22', 22, 18, '新增客户标签', 'add_customer_tag', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:17:26');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a23', 23, 0, '销售工具', 'content', 'icon-xiaoxi', 0, 1, 0, '2021-02-05 17:01:58', '2021-02-06 22:21:52');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a24', 24, 23, '聊天工具栏', '/content/manage', NULL, 0, 1, 0, '2021-02-05 17:01:58', '2021-02-06 22:21:52');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a25', 25, 24, '新增分组', 'content_group_add', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:29:54');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a26', 26, 24, '修改分组', 'content_group_edit', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:29:54');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a27', 27, 24, '删除分组', 'content_group_del', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:29:54');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a28', 28, 24, '内容列表', 'content_list', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:29:54');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a29', 29, 24, '新增内容', 'content_add', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:34:56');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a30', 30, 24, '编辑内容', 'content_edit', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:29:54');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a31', 31, 24, '删除内容', 'content_del', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:29:54');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a32', 32, 0, '企业管理', 'address_book', 'icon-qiyeguanli', 0, 1, 0, '2021-02-05 17:01:58', '2021-02-06 22:21:52');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a33', 33, 32, '员工管理', '/address_book/member_list', NULL, 0, 1, 0, '2021-02-05 17:01:58', '2021-02-06 22:21:52');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a34', 34, 32, '企业标签', '/address_book/label_list', NULL, 0, 1, 0, '2021-02-05 17:01:58', '2021-02-06 22:21:52');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a35', 35, 33, '同步成员行为数据', 'sync_staff_behavior', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:21:52');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a36', 36, 33, '成员列表', 'get_user_simple_list', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:21:52');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a37', 37, 33, '成员打标签/移除标签', 'staff_tagging', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:21:52');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a38', 38, 34, '新增编辑成员标签组', 'add_staff_tag_group', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:21:52');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a39', 39, 34, '删除成员标签组', 'del_staff_tag_group', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:21:52');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a40', 40, 34, '新增编辑成员标签', 'add_staff_tag', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:21:52');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a41', 41, 34, '删除成员标签', 'del_staff_tag', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-06 22:21:52');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a42', 42, 0, '子账户管理', '/account/index', 'icon-zhanghuguanli', 0, 1, 0, '2021-02-05 17:01:58', '2021-09-18 15:05:33');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a43', 43, 42, '子账户管理列表', 'get_user_list', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-08 14:21:04');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a44', 44, 42, '新增角色', 'roles_add', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-08 14:39:12');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a45', 45, 42, '编辑角色', 'roles_edit', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-08 14:39:14');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a46', 46, 42, '删除角色', 'roles_del', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-08 14:39:16');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a47', 47, 42, '编辑子账户', 'user_edit', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-08 14:35:42');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a48', 48, 42, '子账户详情', 'get_user_info', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-08 14:35:42');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a49', 49, 42, '编辑子账户状态', 'user_edit_disable', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-08 14:38:39');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a50', 50, 42, '重置密码', 'user_reset_pwd', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-02-08 14:39:25');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a51', 51, 13, '客户群', '/custom_crm/base', NULL, 0, 1, 1, '2021-02-05 17:01:58', '2021-09-18 15:06:22');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a52', 52, 13, '自动入群', '/custom_crm/in_group', NULL, 0, 1, 1, '2021-02-05 17:01:58', '2021-09-18 15:06:16');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a53', 53, 4, '文章引流', '/guest_tools/article_list', NULL, 0, 1, 1, '2021-02-05 17:01:58', '2021-09-18 15:06:08');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a54', 54, 51, '上传群二维码', 'upload_group_qr_code', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-03-12 16:16:53');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a55', 55, 51, '编辑群二维码', 'edit_customer_group_qr_code', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-03-12 16:16:53');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a56', 56, 51, '新增群激励目标', 'add_customer_group_target', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-03-12 16:19:25');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a57', 57, 51, '编辑群激励目标', 'edit_customer_group_target', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-03-12 16:19:25');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a58', 58, 51, '确认客户群奖励放发', 'customer_group_target_grant', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-03-12 16:25:42');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a59', 59, 5, '新增活码激励目标', 'add_live_qr_target', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-03-12 16:27:36');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a60', 60, 5, '编辑活码激励目标', 'edit_live_qr_target', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-03-12 16:27:43');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a61', 61, 5, '确认活码奖励放发', 'live_qr_target_grant', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-03-12 16:27:43');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a62', 62, 53, '新增文章引流激励目标', 'add_article_target', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-03-12 16:29:48');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a63', 63, 53, '编辑文章引流激励目标', 'edit_article_target', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-03-12 16:29:48');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a64', 64, 53, '文章引流数据统计', 'article_statistics', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-03-12 16:29:48');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a65', 65, 53, '确认文章引流奖励放发', 'article_target_grant', NULL, 0, 0, 0, '2021-02-05 17:01:58', '2021-03-12 16:29:48');
INSERT INTO `sys_module` VALUES ('06388ee3-6e9a-f31e-23a8-2afb47394a73', 73, 13, '群打卡', '/clock', '', 0, 1, 1, '2021-02-05 17:01:58', '2021-09-18 15:06:06');




/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET FOREIGN_KEY_CHECKS = 1 */;


